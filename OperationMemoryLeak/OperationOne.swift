//
//  OperationOne.swift
//  OperationMemoryLeak
//
//  Created by Nick Harris on 2/2/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation

class OperationOne: NSOperation {

    var createdStrings: [String]?
    
    override init() {
        createdStrings = nil
        super.init()
    }
    
    override func main() {
        print("OperationOne - main()")
        createdStrings = ["1", "2", "3", "4", "5"]
    }
    
    deinit {
        print("OperationOne - deinit")
    }
}
