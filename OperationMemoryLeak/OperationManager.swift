//
//  OperationManager.swift
//  OperationMemoryLeak
//
//  Created by Nick Harris on 2/2/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation

class OperationManager {
    
    let operationQueue: NSOperationQueue
    
    init() {
        operationQueue = NSOperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        
        let operationOne = OperationOne()
        let operationTwo = OperationTwo()
        
        let transferOperation = NSBlockOperation() {
            print("transferOperation")
            if let createStrings = operationOne.createdStrings {
                operationTwo.passedInStrings = createStrings
            }
        }
        
        transferOperation.addDependency(operationOne)
        operationTwo.addDependency(transferOperation)
        
        operationQueue.addOperation(operationOne)
        operationQueue.addOperation(transferOperation)
        operationQueue.addOperation(operationTwo)
    }
}