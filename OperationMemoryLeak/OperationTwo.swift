//
//  OperationTwo.swift
//  OperationMemoryLeak
//
//  Created by Nick Harris on 2/2/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation

class OperationTwo: NSOperation {
    
    var passedInStrings: [String]?
    override init() {
        passedInStrings = nil
        super.init()
    }

    override func main() {
        print("OperationTwo - main()")
        if let passedInStrings = passedInStrings {
            for string in passedInStrings {
                print(string)
            }
        }
    }
    
    deinit {
        print("OperationTwo - deinit")
    }
}
