//
//  AppDelegate.swift
//  OperationMemoryLeak
//
//  Created by Nick Harris on 2/2/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var operationManager: OperationManager?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        operationManager = OperationManager()
        // Override point for customization after application launch.
        return true
    }
}

